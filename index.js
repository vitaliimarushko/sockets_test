// Initializing variables
const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

// Describing endpoints
app.use('/:file', (req, res) => {
  res.sendFile(`${__dirname}/${req.params.file}`);
});

// Enabling socket connection and listening events
io.on('connection', socket => {
  socket.on('writemessage', data => {
    io.sockets.emit('addmessage', data);
  });
});

// Running server
server.listen(3000);
