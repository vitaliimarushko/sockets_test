// Initializing variables
const socket = io('http://localhost:3000');
const messagesEl = document.getElementById('messages');
const authorEl = document.getElementById('author');
const messageEl = document.getElementById('message');
const sendMessageEl = document.getElementById('send-message');

// Subscribing on receiving message from socket connection
socket.on('addmessage', data => {
  messagesEl.innerHTML += `<b>${data.author}:</b> ${data.message}<br>`;
  authorEl.value = messageEl.value = '';
});

// Adding handler for sending new message through socket
sendMessageEl.addEventListener('click', () => {
  socket.emit('writemessage', {
    author: authorEl.value,
    message: messageEl.value
  });
});
